# Generated by Django 2.2.3 on 2019-07-22 02:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hardware', '0008_auto_20190719_2305'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hardware',
            name='warranty',
            field=models.IntegerField(blank=True, max_length=1000),
        ),
    ]
