from rest_framework.generics import ListAPIView

from rest_framework.views import APIView
from rest_framework.response import Response


from django.shortcuts import render
from .models import Hardware
from .models import PartType
from .models import Manufacturer

from .serializers import HardwaresSerializer
import json
# Create your views here.

class ListHardwaresView(ListAPIView):
    queryset = Hardware.objects.all()
    serializer_class = HardwaresSerializer

# class ListHardwaresView(APIView):
#     def get(self, request, format=None):
#         hardwares = Hardware.objects.all()
#         result = []
#         for hardware in hardwares:
#             detail = {}

#             detail["model"] = hardware.model

#             partType = hardware.part_type
#             detail["part_type"] = partType.part_name

#             manufacturer = hardware.manufacturer
#             detail["manufacturer"] = manufacturer.manufacturer_name

#             detail["description"] = hardware.description
#             detail["warranty"] = hardware.warranty

#             result.append(detail)
#         return Response(result)
    
#     def post(self, request, format=None):
#         model = request.data["model"]
#         print(request.data["part_type"])
#         partType = PartType.objects.get(part_name=request.data["part_type"]) 
#         manufacturer = Manufacturer.objects.get(manufacturer_name=request.data["manufacturer"])
#         description = request.data["description"]
#         warranty = request.data["warranty"]
        
#         newHardare = Hardware(model=model, part_type=partType, manufacturer=manufacturer, description=description, warranty=warranty)
#         newHardare.save()
#         result = 'OK'
#         return Response(result)

#     def put(self, request, format=None):
#         result = "PUT hardware called"
#         return Response(result)

#     def delete(self, request, format=None):
#         result = "DELETE hardware called"
#         return Response(result)


        
