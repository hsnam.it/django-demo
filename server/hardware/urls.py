from django.urls import path
from .views import ListHardwaresView

urlpatterns = [
    path('hardwares/', ListHardwaresView.as_view(), name="hardwares-all")
]