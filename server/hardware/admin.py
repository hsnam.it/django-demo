from django.contrib import admin
from .models import Warehouse
from .models import Hardware
from .models import Manufacturer
from .models import PartType
from .models import Status
# Register your models here.

admin.site.register(Warehouse)
admin.site.register(Hardware)
admin.site.register(Manufacturer)
admin.site.register(PartType)
admin.site.register(Status)
