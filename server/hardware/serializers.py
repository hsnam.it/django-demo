from rest_framework.serializers import ModelSerializer
from .models import Hardware
from .models import Manufacturer
from .models import PartType
from .models import Warehouse

# Serializers will turn data into JSON type and also do validation tasks on incoming data (like ModelForm in Django Form)

class HardwaresSerializer(ModelSerializer):
    class Meta:
        model = Hardware
        fields = [
            "id",
            "model",
            "part_type",
            "manufacturer",
            "description",
            "warranty"
        ]