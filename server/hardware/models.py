from django.db import models

# Create your models here.


class PartType(models.Model):
    part_name = models.CharField(max_length=255, null=False)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.part_name

class Manufacturer(models.Model):
    manufacturer_name = models.CharField(max_length=255, null=False)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.manufacturer_name

class Hardware(models.Model):
    model = models.CharField(max_length=255, null=False)
    part_type = models.ForeignKey(PartType, on_delete=models.CASCADE)
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE)
    warranty = models.IntegerField(blank=False)
    description = models.TextField(blank=True)
    

    def __str__(self):
        return self.model

class Status(models.Model):
    status_name = models.CharField(max_length=255, null=False)

    def __str__(self):
        return self.status_name

class Warehouse(models.Model):
    sku = models.ForeignKey(Hardware, on_delete=models.CASCADE)
    barcode = models.CharField(max_length=255, null=False)
    status = models.ForeignKey(Status, on_delete=models.PROTECT)
    note = models.TextField(blank=True)

    def __str__(self):
        return self.sku.model

